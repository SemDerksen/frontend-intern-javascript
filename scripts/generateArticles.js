export { fetchArticles };
const articleWrapper = document.querySelector(".article-wrapper");

// Inserts HTML with article object values to body of article wrapper div
function generateArticles(articles) {
    articleWrapper.innerHTML = "";
    articles.forEach( article => {
        articleWrapper.insertAdjacentHTML("beforeend",
            `<article><a href="article/${article.name.toLowerCase()}">
                <img src="${article.image}" alt="${article.name}">
                <h3>${article.name}</h3>
                <h2>${article.description}</h2>
                <button><img src="images/triangle-blue.svg">VIEW CASE</button>
            </a></article>`
        )
        articleWrapper.childNodes.forEach( child => { // Adds fade in transition
            child.classList.add("article-fade-in");
            child.classList.add("visible");
            document.querySelector(".article-fade-in").addEventListener('animationend', () => {
                child.classList.remove("article-fade-in");
            })
        })
    })
};

// Use query string to fetch article data from server.js
function fetchArticles() {
    fetch("http://localhost:3000/articles" + window.location.search)
    .then(res => res.json())
    .then(data => generateArticles(data))
    .catch(err => console.log(err));
};

// Initial fetch upon loading page
fetchArticles();