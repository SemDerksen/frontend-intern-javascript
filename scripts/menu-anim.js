const openMenuButton = document.querySelector(".hamburger");
const menuOverlay = document.querySelector(".menu-overlay");
const closeMenuButton = document.querySelector(".close-menu-button");

openMenuButton.addEventListener("click", () => {
    menuOverlay.classList.add("menu-fade-in");
    document.body.classList.add("fixed");
});

closeMenuButton.addEventListener("click", () => {
    menuOverlay.classList.remove("menu-fade-in");
    document.body.classList.remove("fixed");
});


