import { fetchArticles } from "./generateArticles.js";
const industrySelect = document.getElementById("industry-select");
const typeSelect = document.getElementById("type-select");
const articleWrapper = document.querySelector(".article-wrapper");

// Makes changes to query string in URL with value of selection
function articleFilter() {
    const url = window.location.origin + window.location.pathname; // Base URL
    const industryFilterValue = industrySelect.options[industrySelect.selectedIndex].value; // Value of industry filter selection
    const typeFilterValue = typeSelect.options[typeSelect.selectedIndex].value; // Value of type filter selection

    let str = "?";
    if (industryFilterValue != "all") {
        str += "industry=" + industryFilterValue + "&";
    }
    if (typeFilterValue != "all") {
        str += "type=" + typeFilterValue;
    }

    if (industryFilterValue === "all" && typeFilterValue === "all") {
        history.replaceState({}, "", url); // Returns URL to base state
    }
    else {
        history.replaceState({}, "", url + str); // Adds selected industry and type as query parameters
    }
};

function updateArticles() { // Update filtered articles
    articleWrapper.childNodes.forEach( child => { // Adds fade out transition to each child
        child.classList.add("article-fade-out");
        child.classList.remove("visible");
    })
    setTimeout(function () {
        articleFilter();
        fetchArticles();
    }, 125)
    // articleWrapper.querySelector(".item-fade-out").addEventListener('animationend', () => {})
    // Causes issues with async and concurrency, introducing inability to find .item-fade-out at times and breaking article generation
    // Replaced with setTimeout instead
};

// Update filtered articles on selection change
industrySelect.addEventListener("change", (event) => {
    updateArticles()
});
typeSelect.addEventListener("change", (event) => {
    updateArticles()
});

