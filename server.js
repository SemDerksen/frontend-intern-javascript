const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('articles.json'); // Local JSON file containing object array with article data
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(router);

server.listen(3000, () => {
  console.log("JSON Server: starting server on port 3000");
});

// Filters articles if query strings exist, otherwise returns all articles
server.get('/articles', (req, res) => {
  if (req.query =! null) {
    let filteredArticles = [];

    router.articles.forEach(article => {
      if (article.industry === req.query.industry || article.type === req.query.type) {
        filteredArticles.push(article); // Adds article objects of which key value matches that of the query parameters to array
      }
  });
  res.json(filteredArticles); // Returns array of filtered article objects
  }
  else {
    res.json(router.articles); // Returns array of all article objects
  }
});