# frontend-intern-javascript

Continuation of the assignment for the Dept front-end development internship vacancy, with a heavy focus on JavaScript. This project uses https://github.com/typicode/json-server as a fake REST API and makes use of the HTML and CSS from my previous application assignment, you can find this on: https://gitlab.com/SemDerksen/frontend-intern-assignment

## Installation

Use `nvm`

- [NVM For Windows](https://github.com/coreybutler/nvm-windows)
- [NVM for Linux / Mac](https://github.com/nvm-sh/nvm)

And use Node 16.13.1

```bash
nvm install 16.13.1
nvm use 16.13.1

# confirm your node version
node -v
```

Install the dependencies:

```bash
npm install
# Dependecies used: https://github.com/typicode/json-server
```

Run the server (JSON Server)

```bash
npm start
# You can view the article data on http://localhost:3000/articles
```

Then open index.html with the Live Server plugin for Visual Studio Code (https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) or a comparable environment.